import 'dart:async';

import 'package:flutter/material.dart';
import 'package:heartfish_example/service.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  var _service = ExercisesService();

  void _reset() {
    setState(() {
      _service = ExercisesService();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Exercises'),
          actions: [
            IconButton(
              icon: Icon(Icons.settings_backup_restore),
              onPressed: _reset,
              tooltip: 'Reset',
            ),
          ],
        ),
        body: ExercisesPage(service: _service),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}

class ExercisesPage extends StatefulWidget {
  final ExercisesService service;

  ExercisesPage({Key key, this.service}) : super(key: key);

  @override
  ExercisesPageState createState() => ExercisesPageState();
}

String _nextExerciseChoice;

class ExercisesPageState extends State<ExercisesPage>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  StreamSubscription<num> _heartRateSubscription;

  int _nrRemainingExercises;

  num _heartRate;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: Duration(milliseconds: 150),
      vsync: this,
    );
    _heartRateSubscription = widget.service.watchHeartRate().listen(_heartRateUpdate);
    _nrRemainingExercises = widget.service.getExercises().length;
  }

  void _nextExercise() async {
    await showDialog<String>(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return SimpleDialog(
          title: Text('Choose next exercise'),
          children: widget.service.getExercises()
              .map((exercise) {
                return SimpleDialogOption(
                  onPressed: () {
                    _nextExerciseChoice = exercise;
                    Navigator.pop(context);
                  },
                  child: Text(exercise),
                );
              }).toList(),
        );
      }
    );
    widget.service.doExercise(_nextExerciseChoice);
    _nextExerciseChoice = null;
    _nrRemainingExercises -= 1;
  }

  Future<void> _heartRateUpdate(num heartRate) async {
    _controller.animateTo(0.5);
    _controller.reverse();
    setState(() {
      _heartRate = heartRate;
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (BuildContext context, Widget child) {
        if (_nrRemainingExercises < 0) {
          return Center(
            child: Text('Done!'),
          );
        }
        if (widget.service.getActiveExercise() == null) {
          return Center(
            child: ElevatedButton.icon(
              onPressed: _nextExercise,
              label: Text('Start'),
              icon: Icon(Icons.play_arrow),
            ),
          );
        }
        return DefaultTextStyle(
          style: Theme.of(context).textTheme.headline3,
          child: Center(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(widget.service.getActiveExercise(),
                    style: DefaultTextStyle.of(context).style,
                  ),
                ),
                Text(_nrRemainingExercises.toString() + ' exercises remaining',
                  style: DefaultTextStyle.of(context).style.apply(fontSizeFactor: 0.5)
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Transform.scale(
                    scale: 1 - _controller.value,
                    child: Text(_heartRate.toString()),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: ElevatedButton.icon(
                    onPressed: _nextExercise,
                    label: Text('Next exercise'),
                    icon: Icon(Icons.navigate_next),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
