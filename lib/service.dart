class ExercisesService {
  final _history = <String>[];

  /// Returns a stream which emits the current heart rate of the user
  ///
  /// Throws if the service fails to get the heart rate.
  Stream<num> watchHeartRate() {
    return Stream.periodic(
        Duration(milliseconds: 500),
            (i) {
              if (i % 13 == 12) {
                throw Exception('Unable to get heart rate');
              }
              return _heartRate[i % _heartRate.length];
            });
  }

  /// Returns the list of exercises which are left to be completed
  List<String> getExercises() {
    return _exercises
        .where((exercise) => !_history.contains(exercise))
        .toList();
  }

  /// Marks an exercise as done
  void doExercise(String exercise) {
    assert(exercise != null);
    _history.add(exercise);
  }

  /// Returns current the exercise
  ///
  /// Throws if there is no current exercise.
  String getActiveExercise() {
    return _history.last;
  }
}

const _heartRate = [ 87, 84, 89, 88, 86 ];
const _exercises = ['push ups', 'leg raises', 'squats'];