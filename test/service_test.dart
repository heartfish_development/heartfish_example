import 'package:heartfish_example/service.dart';
import 'package:test/test.dart';

void main() {
  test('watchHeartRate emits positive integers', () {
    final values = ExercisesService().watchHeartRate().take(10).toList();
    expect(values, completion(everyElement(isPositive)));
  });

  test('watchHeartRate throws after 12 items', () {
    final values = ExercisesService().watchHeartRate().take(13).toList();
    expect(values, throwsA(isA<Exception>()));
  });

  test('getExercises non-empty names', () {
    final values = ExercisesService().getExercises();
    expect(values, everyElement(isNotEmpty));
  });

  test('doExercise changes the active exercise', () {
    final service = ExercisesService();
    final exercise = service.getExercises().first;
    service.doExercise(exercise);

    expect(service.getActiveExercise(), exercise);
  });

  test('getExercises returns remaining exercises after one has been done', () {
    final service = ExercisesService();
    final exercises = service.getExercises();
    final exercise = exercises.removeAt(0);
    service.doExercise(exercise);
    final remaining = service.getExercises();

    expect(remaining, orderedEquals(exercises));
  });

  test('getExercises returns no exercises after all have been done', () {
    final service = ExercisesService();
    final exercises = service.getExercises();
    while (exercises.isNotEmpty) {
      final exercise = exercises.removeAt(0);
      service.doExercise(exercise);
    }
    final remaining = service.getExercises();

    expect(remaining, isEmpty);
  });
}